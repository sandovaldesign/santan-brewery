<?php
/**
 * Database Configuration
 *
 * All of your system's database connection settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/DbConfig.php.
 *
 * @see craft\config\DbConfig
 */

return [
    'driver' => getenv('DB_DRIVER', 'mysql'),
    'server' => getenv('DB_SERVER', 'santan-brew-master.cm3w2jqgjy2s.us-east-1.rds.amazonaws.com'),
    'user' => getenv('DB_USER', 'santanbrew'),
    'password' => getenv('DB_PASSWORD', 'sscBw8vxphapgbiVoLMAFgiv'),
    'database' => getenv('DB_DATABASE', 'craft'),
    'schema' => getenv('DB_SCHEMA', 'public'),
    'tablePrefix' => getenv('DB_TABLE_PREFIX'),
    'port' => getenv('DB_PORT', '3306')
];
