<?php
namespace modules\businesslogic\controllers;

use Craft;
use craft\web\Controller;

/**
 * More info about Controllers...
 *
 * https://doublesecretagency.github.io/craft-businesslogic/controllers
 */

/**
 * Business Logic Controller
 *
 * Controller methods get a little more complicated... There are several ways to access them:
 *
 *     1. Submitting a form can trigger a controller action.
 *     2. Using an AJAX request can trigger a controller action.
 *     3. Routing to an action URL will trigger a controller action.
 *
 * A controller can do many things, but be wary... If your logic gets too complex, you may want
 * to off-load much of it to the Service file.
 */

class VipController extends Controller
{

    /**
     * By default, access to controllers is restricted to logged-in users.
     * However, you can allow anonymous access by uncommenting the line below.
     *
     * It is also possible to allow anonymous access to only certain methods,
     * by supplying an array of method names, rather than a boolean value.
     *
     * See also:
     * https://docs.craftcms.com/api/v3/craft-web-controller.html#allowanonymous
     */
    protected $allowAnonymous = true;

    /**
     * When you need AJAX, this is how to do it.
     *
     * HOW TO USE IT
     * In your front-end JavaScript, POST your AJAX call like this:
     *
     *     // example uses jQuery
     *     $.post('actions/business-logic/example/example-ajax' ...
     *
     * Or if your module is doing something within the control panel,
     * you've got a built-in function available which Craft provides:
     *
     *     Craft.postActionRequest('business-logic/example/example-ajax' ...
     *
     */
    public function actionExampleAjax()
    {
        $this->requirePostRequest();
        $this->requireAcceptsJson();
        $request = Craft::$app->getRequest();
        $brands = $request->getBodyParam('brands');
        $storeType = $request->getBodyParam('storeType');
        $miles = $request->getBodyParam('miles');
        $zip = $request->getBodyParam('zip');


        $custID = 'SAN';
		$parms = 'action=results&zip='.urlencode($zip).'&pagesize=20';

		if ($brands !== "off" | $brands !== "") {
			$parms .= "&brand=".urlencode($brands);
		}

		if ($storeType !== "off") {
			$parms .= "&storeType=".urlencode($storeType);
		}

		if ($miles !== "off") {
			$parms .= "&miles=".urlencode($miles);
		}


		$secret = '537D9FD8E731SAN2A796FCAA90B';

		date_default_timezone_set('GMT');
		$stamp = date("D, j M Y H:i:00 T", time());
		$url = "https://www.vtinfo.com/PF/product_finder-service.asp?".$parms;
		$sigString = $stamp. $secret . $parms . $custID;
		$sigHash = hash('sha256',$sigString);

		$fields = array(
			'action' => rawurlencode('results'),
			'zip' => rawurlencode($zip),
			'pagesize' => rawurlencode(20),
			'page' => rawurlencode($page)
		);

		if ($brands !== "off") {
			$fields['brand'] = rawurlencode($brands);
		}

		// if (isset($_POST['page'] && !empty($_POST['page'])) {
		// 	$fields['page'] = rawurlencode($_POST['page']);
		// }

		if ($storeType !== "off") {
			$fields['storeType'] = rawurlencode($storeType);
		}
		if ($miles !== "off") {
			$fields['miles'] = rawurlencode($miles);
		}

		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'vipCustID: '. $custID,
			'vipTimestamp: '. $stamp,
			'vipSignature: '. $sigHash
		));

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		// echo '<pre>', htmlentities($result), '</pre><br>';

		// $errors = "";
		$xml = simplexml_load_string($result);

		// var_dump($result);
		if ($xml === false) {
		    echo "Failed loading XML: ";
		    foreach(libxml_get_errors() as $error) {
		        echo "<br>", $error->message;
		    }

		} else {

			$resultCounter = 0;

			$results = array();
			$results['general'] = array(
				'total'=>$xml->total->__toString(),
				'miles'=>$xml->input->miles->__toString(),
				'page'=>$xml->page->__toString(),
				'start'=>$xml->start->__toString(),
				'end'=>$xml->end->__toString(),
				'totalPages'=>ceil($xml->total/20)
			);
			$results['results'] = array();

			foreach ($xml->locations->location as $location) {
				$storeType = (string)$location->storeType;
				$distance = $location->distance->__toString();
				// echo $storeType."<br>";

				if (!array_key_exists($distance,$results['results'])) {
					$results['results'][$distance] = array();
				}

				$brands = array();
				foreach ($location->otherBrands as $brand) {
					foreach ($brand as $b) {
						$brands[] = $b->__toString();
					}
					
				}
				$currentResult = array(
					'name'=>$location->dba->__toString(),
					'street'=>$location->street->__toString(),
					'city'=>$location->city->__toString(),
					'state'=>$location->state->__toString(),
					'zip'=>$location->zip->__toString(),
					'lat'=>(float)$location->lat->__toString(),
					'long'=>(float)$location->long->__toString(),
					'distance'=>$distance,
					'phone'=>$location->phone->__toString(),
					'brands'=>$brands
				);

				$results['results'][$distance][] = $currentResult;

				$resultCounter++;

				// echo(json_encode($currentResult, true));

				// echo $location->dba;
			}

			$results['general']['resultAmount'] = $resultCounter;

			foreach ($results['results'] as $dis=>$result) {

				// Sort by name:
				array_multisort( array_column($results['results'][$dis], "name"), SORT_ASC, $results['results'][$dis] );

				// Sort by weight:
				// array_multisort( array_column($array, "weight"), SORT_ASC, $array );
			}

		    // print_r($xml);
		    // echo json_encode($results, true);
		    return $this->asJson($results);
		    // echo '<pre>', htmlentities(json_encode($results, JSON_PRETTY_PRINT)), '</pre><br>';
		}

        // ... whatever your AJAX does...
        // $response = ['response' => 'Round trip via AJAX!'];
        // return $this->asJson($response);
    }

    /**
     * Routing lets you set extra variables when you load a Twig template.
     *
     * HOW TO USE IT
     * Put this in your config/routes.php file:
     *
     *     'your/route' => 'business-logic/example/example-route'
     *
     * Optionally, you can specify dynamic parameters as part of the route:
     *
     *     'your/route/<lorem:\d+>/<ipsum:\d+>' => 'business-logic/example/example-route'
     *
     * If you specify dynamic parameters, pass those values directly into your function.
     * The variable names must match the specified tokens.
     *
     *     actionExampleRoute($lorem, $ipsum)
     *
     */
    public function actionExampleRoute()
    {
        // ... whatever your route accomplishes...
        $twigVariable = 'I added this with a route!';
        return $this->renderTemplate('your/destination/template', [
            'twigVariable' => $twigVariable
        ]);
    }

}
