<?php
namespace modules\businesslogic\controllers;

use Craft;
use craft\web\Controller;

/**
 * More info about Controllers...
 *
 * https://doublesecretagency.github.io/craft-businesslogic/controllers
 */

/**
 * Business Logic Controller
 *
 * Controller methods get a little more complicated... There are several ways to access them:
 *
 *     1. Submitting a form can trigger a controller action.
 *     2. Using an AJAX request can trigger a controller action.
 *     3. Routing to an action URL will trigger a controller action.
 *
 * A controller can do many things, but be wary... If your logic gets too complex, you may want
 * to off-load much of it to the Service file.
 */

class ExampleController extends Controller
{

    /**
     * By default, access to controllers is restricted to logged-in users.
     * However, you can allow anonymous access by uncommenting the line below.
     *
     * It is also possible to allow anonymous access to only certain methods,
     * by supplying an array of method names, rather than a boolean value.
     *
     * See also:
     * https://docs.craftcms.com/api/v3/craft-web-controller.html#allowanonymous
     */
    protected $allowAnonymous = true;

    /**
     * For a normal form submission, send it here.
     *
     * HOW TO USE IT
     * The HTML form in your template should include this hidden field:
     *
     *     <input type="hidden" name="action" value="business-logic/example/example-form-submit">
     *
     * The action path follows this format:
     *
     *     business-logic/<CONTROLLER HANDLE>/<FUNCTION HANDLE>
     *
     * In this case, the handles should be kebab-cased, and omit "Controller" and "action" (respectively).
     *
     */
    public function actionExampleFormSubmit()
    {
        $this->requirePostRequest();
        $request = Craft::$app->getRequest();
        $lorem = $request->getBodyParam('lorem');
        $ipsum = $request->getBodyParam('ipsum');
        // ... whatever you want to do with the submitted data...
        $this->redirect('thank/you/page');
    }

    /**
     * When you need AJAX, this is how to do it.
     *
     * HOW TO USE IT
     * In your front-end JavaScript, POST your AJAX call like this:
     *
     *     // example uses jQuery
     *     $.post('actions/business-logic/example/example-ajax' ...
     *
     * Or if your module is doing something within the control panel,
     * you've got a built-in function available which Craft provides:
     *
     *     Craft.postActionRequest('business-logic/example/example-ajax' ...
     *
     */
    public function actionExampleAjax()
    {
        $this->requirePostRequest();
        $this->requireAcceptsJson();
        $request = Craft::$app->getRequest();
        $lorem = $request->getBodyParam('lorem');
        $ipsum = $request->getBodyParam('ipsum');
        // ... whatever your AJAX does...
        $response = ['response' => 'Round trip via AJAX!'];
        return $this->asJson($response);
    }

    public function actionVipAjax()
    {
        $this->requirePostRequest();
        // $this->requireAcceptsJson();
        $request = Craft::$app->getRequest();
        $brands = $request->getBodyParam('brands');
        $storeType = $request->getBodyParam('storeType');
        $miles = $request->getBodyParam('miles');
        $zip = $request->getBodyParam('zip');
        $page = $request->getBodyParam('page');


        $custID = 'SAN';
        $parms = 'action=results&zip='.urlencode($zip).'&pagesize=20';

        if ($brands !== "off" | $brands !== "") {
            $parms .= "&brand=".urlencode($brands);
        }

        if ($storeType !== "off") {
            $parms .= "&storeType=".urlencode($storeType);
        }

        if ($miles !== "off") {
            $parms .= "&miles=".urlencode($miles);
        }


        $secret = '537D9FD8E731SAN2A796FCAA90B';

        date_default_timezone_set('GMT');
        $stamp = date("D, j M Y H:i:00 T", time());
        $url = "https://www.vtinfo.com/PF/product_finder-service.asp?".$parms;
        $sigString = $stamp. $secret . $parms . $custID;
        $sigHash = hash('sha256',$sigString);

        $fields = array(
            'action' => rawurlencode('results'),
            'zip' => rawurlencode($zip),
            'pagesize' => rawurlencode(20),
            'page' => rawurlencode($page)
        );

        if ($brands !== "off") {
            $fields['brand'] = rawurlencode($brands);
        }

        // if (isset($_POST['page'] && !empty($_POST['page'])) {
        //  $fields['page'] = rawurlencode($_POST['page']);
        // }

        if ($storeType !== "off") {
            $fields['storeType'] = rawurlencode($storeType);
        }
        if ($miles !== "off") {
            $fields['miles'] = rawurlencode($miles);
        }

        $fields_string = "";

        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'vipCustID: '. $custID,
            'vipTimestamp: '. $stamp,
            'vipSignature: '. $sigHash
        ));

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
        // echo '<pre>', htmlentities($result), '</pre><br>';

        // $errors = "";
        $xml = simplexml_load_string($result);

        // var_dump($result);
        if ($xml === false) {
            echo "Failed loading XML: ";
            foreach(libxml_get_errors() as $error) {
                echo "<br>", $error->message;
            }

        } else {

            $resultCounter = 0;

            $results = array();
            $results['general'] = array(
                'total'=>$xml->total->__toString(),
                'miles'=>$xml->input->miles->__toString(),
                'page'=>$xml->page->__toString(),
                'start'=>$xml->start->__toString(),
                'end'=>$xml->end->__toString(),
                'totalPages'=>ceil($xml->total/20)
            );
            $results['results'] = array();

            foreach ($xml->locations->location as $location) {
                $storeType = (string)$location->storeType;
                $distance = $location->distance->__toString();
                // echo $storeType."<br>";

                if (!array_key_exists($distance,$results['results'])) {
                    $results['results'][$distance] = array();
                }

                $brands = array();
                foreach ($location->otherBrands as $brand) {
                    foreach ($brand as $b) {
                        $brands[] = $b->__toString();
                    }
                    
                }
                $currentResult = array(
                    'name'=>$location->dba->__toString(),
                    'street'=>$location->street->__toString(),
                    'city'=>$location->city->__toString(),
                    'state'=>$location->state->__toString(),
                    'zip'=>$location->zip->__toString(),
                    'lat'=>(float)$location->lat->__toString(),
                    'long'=>(float)$location->long->__toString(),
                    'distance'=>$distance,
                    'phone'=>$location->phone->__toString(),
                    'brands'=>$brands
                );

                $results['results'][$distance][] = $currentResult;

                $resultCounter++;

                // echo(json_encode($currentResult, true));

                // echo $location->dba;
            }

            $results['general']['resultAmount'] = $resultCounter;

            foreach ($results['results'] as $dis=>$result) {

                // Sort by name:
                array_multisort( array_column($results['results'][$dis], "name"), SORT_ASC, $results['results'][$dis] );

                // Sort by weight:
                // array_multisort( array_column($array, "weight"), SORT_ASC, $array );
            }

            // print_r($xml);
            // echo json_encode($results, true);
            return $this->asJson($results);
            // echo '<pre>', htmlentities(json_encode($results, JSON_PRETTY_PRINT)), '</pre><br>';
        }

        // ... whatever your AJAX does...
        // $response = ['response' => 'Round trip via AJAX!'];
        // return $this->asJson($response);
    }

    public function actionLocuAjax() 
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.locu.com/v2/venue/search",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\n\t\"api_key\": \"1a1f648606818eed274fa0fab5898071aea6ed54\",\n\t\"fields\":[\"name\", \"menus\"],\n\t\"venue_queries\": [\n\t\t{\n\t\t\t\"locu_id\" : \"56e08035e0fba8c24645\"\n\t\t}\n\t]\n}",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          // echo json_encode($response, true);
          return $this->asJson($response);
        }

    }

    public function actionSendgridAjax()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/

        $this->requirePostRequest();
        // $this->requireAcceptsJson();
        $success = false;
        $request = Craft::$app->getRequest();
        $mailto = explode(',', $request->getBodyParam('mailto'));
        $mailSubject = $request->getBodyParam('formTitle');
        $formType = $request->getBodyParam('formType');
        $menuLink = $request->getBodyParam('menuLink');
        $mailBody = "New {$mailSubject} Submission: \r\n";
        $replyTo = '';



        $addresses = array();

        foreach ($mailto as $key => $value) {
            $string = preg_replace('/\s+/', '', $value);
            $addresses[] = array('name'=>'Santan Employee', 'email'=>$string);
        }



        $post_fields = array(
            "personalizations" => array(
                array(
                    "to" => $addresses
                )
            ),
            "subject" => $mailSubject,
            "from" => array(
                "email" => "notifications@sandovaldesign.com",
                "name" => "San Tan Form Submission"
            )
        );

        if ($formType == 'catering') {

            $replyTo = $request->getBodyParam('Email');
            $location = $request->getBodyParam('Location_of_Event');
            $other_location = $request->getBodyParam('If_Other_Location');

            if ($other_location !== "") {
                $location = $other_location;
            }

            $mailBody = "
                Full Name: {$request->getBodyParam('Full_Name')},<br>
                Email: {$request->getBodyParam('Email')},<br>
                Phone Number: {$request->getBodyParam('Phone_Number')},<br>
                Date: {$request->getBodyParam('Pick_a_Date')},<br>
                Time: {$request->getBodyParam('Time')},<br>
                Location of Event: {$location},<br>
                Guests: {$request->getBodyParam('Number_of_Guests')},<br>
                Comments: {$request->getBodyParam('Comments_and_additional_requests')},<br>
                Catering Type: {$mailSubject}
            ";

            $post_fields['reply_to'] = array('email'=>$replyTo);
            $post_fields['content'] = array(array('type'=>'text/html', 'value'=> $mailBody));

            $receipt_subject = "Catering Request Confirmation";
            $reciept_addresses = array(array('email'=>$replyTo));

            $receipt_post_fields = array(
                "personalizations" => array(
                    array(
                        "to" => $reciept_addresses,
                        "bcc" => array(
                            array("email"=> "catering@santanbrewing.com"), 
                            array("email" => "jamie@santanbrewing.com"), 
                            array("email" => "kaitlin@santanbrewing.com")
                        )
                    )
                ),
                "subject" => $receipt_subject,
                "from" => array(
                    "email" => "notify@sandovaldesign.com",
                    "name" => "San Tan Brewing Notifications"
                )
            );

            $receipt_copy = "Hi ".$request->getBodyParam('Full_Name')."!<br><br>

                    Thank you for reaching out to our catering team for your event at ".$location." on ".$request->getBodyParam('Pick_a_Date').". Our team is reviewing your request and checking availability, and will get back to you asap (but please allow up to 24-48 hours for a response!)<br>

                    In the meantime, check out the <a href=\"".$menuLink."\">attached information</a> for packages, pricing, and menus. <br><br>

                    - SanTan Catering Team<br><br><br>

                    <small>If you cannot click the above link, enter this URL in your browser: ".$menuLink."</small>";

            $receipt_post_fields['content'] = array(array('type'=>'text/html', 'value'=> $receipt_copy));

        } elseif ($formType == 'kegOrder') {
            $kegDetails = "";
            $kegArray = [];
            $kegDetails = $request->getBodyParam('kegDetails');
            $tap = $request->getBodyParam('tap');
            $bucket = $request->getBodyParam('bucket');
            // return json_encode($kegDetails);
            foreach ($kegDetails as $key=>$value) {
                $myArray = array();
                foreach ($value as $kk=>$vv) {
                    if ($kk == "size") {
                        $myArray['size'] = $vv;
                    } elseif ($kk == "beerType") {
                        $myArray['beer'] = $vv;
                    }
                }
                $kegArray[] = $myArray;
            }

            // return json_encode($kegArray);

            $replyTo = $request->getBodyParam('email');
            $fullName = $request->getBodyParam('full-name');
            $phone = $request->getBodyParam('phone');
            $date = $request->getBodyParam('date');
            $comments = $request->getBodyParam('comments');

            // $email->setSubject("Keg Order Confirmation");

            $recieptTap = "No";
            $recieptBucket = "No";
            if ($tap == 'on') {
                $recieptTap = "Yes";
            }
            if ($bucket == 'on') {
                $recieptBucket = "Yes";
            }

            $substitutions = array(
                "name" => $fullName,
                "date" => $date,
                "bucket" => $recieptBucket,
                "tap" => $recieptTap,
                "user" => array(
                    "kegOrder"=> $kegArray
                ),
                "comments" => $comments,
                "phone" => $phone
            );

            $post_fields['personalizations'][0]['dynamic_template_data'] = $substitutions;
            $post_fields['reply_to'] = array('email'=>$replyTo);
            $post_fields['template_id'] = "d-c9ea2388049c4c92a894cfae9e0de6de";

            $receipt_subject = "Keg Order Confirmation";
            $reciept_addresses = array(array('email'=>$replyTo));

            $receipt_post_fields = array(
                "personalizations" => array(
                    array(
                        "to" => $reciept_addresses,
                        "bcc" => array(
                            array(
                            "email"=> "stephen@santanbrewing.com"
                            ), 
                            array("email" => "pubbrewerchandler@santanbrewing.com"), 
                            array("email" => "sam@sandovaldesign.com"), 
                            array("email" => "jade@santanbrewing.com"), 
                            array("email" => "joseph@santanbrewing.com"), 
                            array("email" => "kelly@santanbrewing.com"), 
                            array("email" => "brad@santanbrewing.com"), 
                            array("email" => "typhaney@sandovaldesign.com")
                        )
                    )
                ),
                "subject" => $receipt_subject,
                "from" => array(
                    "email" => "notify@sandovaldesign.com",
                    "name" => "San Tan Brewing Notifications"
                ),
                "template_id" => "d-c9ea2388049c4c92a894cfae9e0de6de"
            );
            $receipt_post_fields['personalizations'][0]['dynamic_template_data'] = $substitutions;

        } else {

            $replyTo = $request->getBodyParam('Email');
            $fullName = $request->getBodyParam('Name');
            $phone = $request->getBodyParam('Phone');
            $date = $request->getBodyParam('Date_and_Time');
            $organization = $request->getBodyParam('Organization_Name');
            $location = $request->getBodyParam('Event_Location');
            $website = $request->getBodyParam('Event_Website');
            $type = $request->getBodyParam('Event_Type');
            $guests = $request->getBodyParam('Guests');
            $event = $request->getBodyParam('Tell_us_about_your_event');
            $donate_type = $request->getBodyParam('Donation_Type');
            $beer_requests = $request->getBodyParam('If_requesting_beer,_check_all_that_apply');
            $fed_id = $request->getBodyParam('Federal_Tax_I_D_');

            // $replyTo = $_POST['Email'];

            $mailBody = "
                Full Name: {$fullName},<br>
                Email: {$replyTo},<br>
                Phone Number: {$phone},<br>
                Date: {$date},<br>
                Organization Name: {$organization},<br>
                Event Location: {$location},<br>
                Event Website: {$website},<br>
                Event Type: {$type},<br>
                Guests: {$guests},<br>
                Event Description: {$event},<br>
                Donation Type: {$donate_type},<br>
                Beer Requests: {$beer_requests},<br>
                Fed Tax ID: {$fed_id},<br>
            ";

            $post_fields['reply_to'] = array('email'=>$replyTo);
            $post_fields['content'] = array(array('type'=>'text/html', 'value'=> $mailBody));

        }



        $ch = curl_init();

        $fields_string = json_encode($post_fields);

        curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = 'Authorization: Bearer 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';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            die('Error:' . curl_error($ch));
        } else {
            curl_close($ch);


            if ($formType == 'catering' || $formType == 'kegOrder') {
                // return "hi";
                $ch = curl_init();

                $receipt_fields_string = json_encode($receipt_post_fields);
                // return $receipt_fields_string;

                curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $receipt_fields_string);
                curl_setopt($ch, CURLOPT_POST, 1);

                $headers = array();
                $headers[] = 'Authorization: Bearer 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';
                $headers[] = 'Content-Type: application/json';
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $receipt_result = curl_exec($ch);
                if (curl_errno($ch)) {
                    die('Error:' . curl_error($ch));
                } else {
                    $result = $receipt_result;
                    $success = true;
                }
            } else {
                $success = true;
                
            }
            
        }
        
        return $this->asJson(["success"=>$success, 'version'=>'v3','response'=>$result]);


        
    }

    /**
     * Routing lets you set extra variables when you load a Twig template.
     *
     * HOW TO USE IT
     * Put this in your config/routes.php file:
     *
     *     'your/route' => 'business-logic/example/example-route'
     *
     * Optionally, you can specify dynamic parameters as part of the route:
     *
     *     'your/route/<lorem:\d+>/<ipsum:\d+>' => 'business-logic/example/example-route'
     *
     * If you specify dynamic parameters, pass those values directly into your function.
     * The variable names must match the specified tokens.
     *
     *     actionExampleRoute($lorem, $ipsum)
     *
     */
    public function actionExampleRoute()
    {
        // ... whatever your route accomplishes...
        $twigVariable = 'I added this with a route!';
        return $this->renderTemplate('your/destination/template', [
            'twigVariable' => $twigVariable
        ]);
    }

}
