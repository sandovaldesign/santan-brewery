# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Local Development ###

* First, you must download [Laravel Valet](https://laravel.com/docs/5.6/valet) on your mac. If you are using Windows, we will need to follow different steps.
* After making a directory to host all your local sites, `cd` into said directory and run `valet park` from the Terminal. This will set Valet to serve all folders in the directory as local ".dev" sites.
* Now run `mkdir santan-brew-local && cd santan-brew-local`. This will be where we store the local version of this site.
* `git pull` the master branch.
* With your favorite text editor, let's edit the ".env" file, which holds all the Environment variables for the site. We need to tell your local dev to use the staging database instead of the master database.

### Ready to code ###

* Create a new branch `your_initials-feature`.
* Make your changes.
* Push the changes to your newly created branch.
* After review, the branch will then be merged into the staging branch.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact